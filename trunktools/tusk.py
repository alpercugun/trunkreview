#!/usr/bin/env python3
import os
import sys
import requests
from urllib.parse import urljoin

from git import Repo
import gitlab

from dotenv import load_dotenv
load_dotenv(verbose=True)


# Has to be a token with API scope or this won't work
private_token = os.environ.get("PRIVATE_TOKEN", "notoken")

# https://python-gitlab.readthedocs.io/en/stable/gl_objects/projects.html
gl = gitlab.Gitlab('https://gitlab.com', private_token=private_token)

print(gl.projects.list())
print(gl.projects.get('alpercugun/trunkreview'))

BASE_URL = "https://gitlab.com/api/v4/"

# https://gitlab.com/api/v4/projects/alpercugun%2Ftrunkreview/repository/commits

# Figure out which repo we are running in
# git remote -v

repo = Repo(os.getcwd())
assert not repo.bare

print(repo.remotes[0].name)
print(list(repo.remotes[0].urls))
print(repo.working_tree_dir)
print(repo.config_reader().get(section='user', option='name'))

sys.exit()


project_name = "trunkreview"

project_path = "alpercugun%2Ftrunkreview"

# Figure out which Git username I am
# git config --get "user.name"
# user.name=Alper Çugun
# user.email=github@alper.nl

# Retrieve last X commits for that repo
# https://docs.gitlab.com/ee/api/commits.html#list-repository-commits
# GET /projects/:id/repository/commits

commits_url = urljoin(BASE_URL, f'projects/{project_path}/repository/commits')

commits = requests.get(commits_url).json()

for commit in commits:
    commit_id = commit.get('id', '')

    comments_url = urljoin(BASE_URL, f'projects/{project_path}/repository/commits/{commit_id}/comments')

    comments = requests.get(comments_url).text

    print(comments)

# Get the comments of each commit
# https://docs.gitlab.com/ee/api/commits.html#get-the-comments-of-a-commit
# GET /projects/:id/repository/commits/:sha/comments

def main():
    print("Script run")